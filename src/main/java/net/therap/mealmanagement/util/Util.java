package net.therap.mealmanagement.util;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author shahriar
 * @since 2/15/18
 */
public class Util {

    public static final String USER = "user";
    public static final String LOGIN_ERROR = "Username or password does not match";

    public static final String VALID_ERROR_DAY = "It is not a valid day";
    public static final String VALID_ERROR_SLOT = "It is not a valid slot";
    public static final String VALID_ERROR_USERNAME_EXIST = "Username already exists";
    public static final String VALID_ERROR_PASSWORD = "The password fields must match";
    public static final String VALID_ERROR_USERNAME = "Username should be minimum of {min} characters";


    public static String getHashedPassword(String password) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        byte[] digest = md.digest();

        return DatatypeConverter.printHexBinary(digest).toUpperCase();
    }
}
