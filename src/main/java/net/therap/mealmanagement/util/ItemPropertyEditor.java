package net.therap.mealmanagement.util;

import net.therap.mealmanagement.domain.Item;

import java.beans.PropertyEditorSupport;

public class ItemPropertyEditor extends PropertyEditorSupport {

    @Override
    public void setAsText(String text) throws IllegalArgumentException {

        Item item = new Item();

        item.setId(Integer.parseInt(text));

        setValue(item);
    }

    @Override
    public String getAsText() {

        Item item = (Item) super.getValue();

        return (item != null) ? String.valueOf(item.getId()) : "";
    }
}
