package net.therap.mealmanagement.controller;

import net.therap.mealmanagement.dao.MealDao;
import net.therap.mealmanagement.domain.Day;
import net.therap.mealmanagement.domain.Meal;
import net.therap.mealmanagement.domain.Slot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @author shahriar
 * @since 3/4/18
 */
@Controller
public class MealController {

    private MealDao mealDao;

    @Autowired
    public MealController(MealDao mealDao) {
        this.mealDao = mealDao;
    }

    @RequestMapping(value = "/private/ViewAllMeal", method = RequestMethod.GET)
    public String viewAllMeal(@ModelAttribute("meal") Meal meal, Model model) {

        model.addAttribute("allMeals", mealDao.getAll());

        return "meal";
    }

    @RequestMapping(value = "/private/ViewMealBySlot", method = RequestMethod.POST)
    public String viewMealBySlot(@ModelAttribute("meal") Meal meal, Model model) {

        model.addAttribute("allMeals", mealDao.getMealBySlot(meal.getDay(), meal.getSlot()));

        return "meal";
    }

    @ModelAttribute("dayList")
    public List<Day> populateDayList() {

        return Day.getConcernedDays();
    }

    @ModelAttribute("slotList")
    public Slot[] populateSlotList() {

        return Slot.values();
    }
}
