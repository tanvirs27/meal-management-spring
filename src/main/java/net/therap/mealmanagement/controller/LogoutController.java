package net.therap.mealmanagement.controller;


import net.therap.mealmanagement.util.Util;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author shahriar
 * @since 2/20/18
 */
@Controller
public class LogoutController {

    @RequestMapping(value = "/public/Logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request)
            throws ServletException, IOException {

        HttpSession session = request.getSession();

        session.removeAttribute(Util.USER);
        session.invalidate();

        return "redirect:/public/Login";
    }
}