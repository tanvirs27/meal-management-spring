package net.therap.mealmanagement.controller;

import net.therap.mealmanagement.dao.UserDao;
import net.therap.mealmanagement.domain.User;
import net.therap.mealmanagement.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author shahriar
 * @since 3/4/18
 */
@Controller
@RequestMapping(value = {"/", "/public/Login"})
public class LoginController {

    private UserDao userDao;

    @Autowired
    public LoginController(UserDao userDao) {
        this.userDao = userDao;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getLoginForm(@ModelAttribute("user") User user) {

        return "login";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String loginUser(@ModelAttribute("user") User user, Model model, HttpServletRequest request) {

        HttpSession session = request.getSession();

        if (userDao.authenticateUser(user.getUsername(), user.getPassword())) {

            session.setAttribute("user", userDao.getByUsername(user.getUsername()));
            return "redirect:/private/ViewAllMeal";
        } else {

            model.addAttribute("error", Util.LOGIN_ERROR);
            return "login";
        }
    }
}
