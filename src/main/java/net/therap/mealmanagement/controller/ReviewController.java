package net.therap.mealmanagement.controller;

import net.therap.mealmanagement.dao.MealDao;
import net.therap.mealmanagement.dao.ReviewDao;
import net.therap.mealmanagement.domain.*;
import net.therap.mealmanagement.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * @author shahriar
 * @since 3/4/18
 */
@Controller
public class ReviewController {

    private ReviewDao reviewDao;
    private MealDao mealDao;

    @Autowired
    public ReviewController(ReviewDao reviewDao, MealDao mealDao) {
        this.reviewDao = reviewDao;
        this.mealDao = mealDao;
    }

    @RequestMapping(value = "/private/ShowAllReview", method = RequestMethod.GET)
    public String viewAllReview(@ModelAttribute("review") Review review, Model model) {

        model.addAttribute("allReviews", reviewDao.getAll());

        return "review";
    }

    @RequestMapping(value = "/private/ShowReviewByMeal", method = RequestMethod.POST)
    public String showReviewByMeal(@ModelAttribute("review") Review review, HttpServletRequest request, Model model) {

        String day = review.getMeal().getDay();
        String slot = review.getMeal().getSlot();

        User user = (User) request.getSession().getAttribute(Util.USER);

        model.addAttribute("allReviews", reviewDao.getReviewByMeal(day, slot, user));

        return "review";
    }

    @RequestMapping(value = "/private/ShowReviewByUser", method = RequestMethod.POST)
    public String viewReviewByMeal(@ModelAttribute("review") Review review, Model model) {

        model.addAttribute("allReviews", reviewDao.getReviewByUser(review.getReviewer().getUsername()));

        return "review";
    }

    @RequestMapping(value = "/private/AddReviewForm", method = {RequestMethod.POST, RequestMethod.GET})
    public String getAddReviewForm(@ModelAttribute("review") Review review) {

        return "addReview";
    }

    @RequestMapping(value = "/private/AddReview", method = RequestMethod.POST)
    public String addReview(@ModelAttribute("review") @Valid Review review, Errors errors, HttpServletRequest request) {

        if (errors.hasErrors()) {
            return "addReview";
        }

        String day = review.getMeal().getDay();
        String slot = review.getMeal().getSlot();

        User user = (User) request.getSession().getAttribute(Util.USER);

        Meal meal = mealDao.getMeal(day, slot, user);

        reviewDao.addReviewToMeal(meal, review.getRating(), review.getComment(), user);

        return "redirect:/private/ShowAllReview";
    }

    @ModelAttribute("dayList")
    public List<Day> populateDayList() {

        return Day.getConcernedDays();
    }

    @ModelAttribute("slotList")
    public Slot[] populateSlotList() {

        return Slot.values();
    }
}
