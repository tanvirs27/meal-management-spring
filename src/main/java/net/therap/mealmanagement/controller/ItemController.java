package net.therap.mealmanagement.controller;

import net.therap.mealmanagement.dao.ItemDao;
import net.therap.mealmanagement.dao.MealDao;
import net.therap.mealmanagement.domain.*;
import net.therap.mealmanagement.util.ItemPropertyEditor;
import net.therap.mealmanagement.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * @author shahriar
 * @since 3/5/18
 */
@Controller
public class ItemController {

    private MealDao mealDao;
    private ItemDao itemDao;

    @Autowired
    public ItemController(MealDao mealDao, ItemDao itemDao) {
        this.mealDao = mealDao;
        this.itemDao = itemDao;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {

        binder.registerCustomEditor(Item.class, "item", new ItemPropertyEditor());
    }

    @RequestMapping(value = "/private/AddItemForm", method = {RequestMethod.POST, RequestMethod.GET})
    public String getAddItemForm(@ModelAttribute("meal") Meal meal) {

        return "addItem";
    }

    @RequestMapping(value = "/private/AddItem", method = RequestMethod.POST)
    public String addItem(@ModelAttribute("meal") @Valid Meal meal, Errors errors, HttpServletRequest request) {

        if (errors.hasErrors()) {
            return "addItem";
        }

        User user = (User) request.getSession().getAttribute(Util.USER);

        String day = meal.getDay();
        String slot = meal.getSlot();
        String name = meal.getItem().getName();

        mealDao.addItemToMeal(day, slot, name.trim(), user);

        return "redirect:/private/ViewAllMeal";
    }

    @RequestMapping(value = "/private/RemoveItem", method = RequestMethod.POST)
    public String removeItem(@ModelAttribute("meal") Meal meal, HttpServletRequest request) {

        User user = (User) request.getSession().getAttribute(Util.USER);

        int mealId = meal.getId();
        int itemId = meal.getItem().getId();

        mealDao.deleteItemFromMeal(mealId, itemId, user);

        return "redirect:/private/ViewAllMeal";
    }

    @RequestMapping(value = "/private/UpdateItemForm", method = RequestMethod.POST)
    public String getUpdateItemForm(@ModelAttribute("meal") Meal meal, Model model) {

        Item item = itemDao.getById(meal.getItem().getId());

        if (item == null) {
            return "redirect:/private/ViewAllMeal";
        }

        model.addAttribute("item", item);

        return "updateItem";
    }

    @RequestMapping(value = "/private/UpdateItem", method = RequestMethod.POST)
    public String updateItem(@ModelAttribute("item") @Valid Item item, Errors errors) {

        if (errors.hasErrors()) {
            return "updateItem";
        }

        itemDao.updateItem(item.getId(), item.getName());

        return "redirect:/private/ViewAllMeal";
    }

    @ModelAttribute("dayList")
    public List<Day> populateDayList() {

        return Day.getConcernedDays();
    }

    @ModelAttribute("slotList")
    public Slot[] populateSlotList() {

        return Slot.values();
    }
}
