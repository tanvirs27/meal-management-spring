package net.therap.mealmanagement.controller;

import net.therap.mealmanagement.dao.UserDao;
import net.therap.mealmanagement.domain.User;
import net.therap.mealmanagement.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.NoSuchAlgorithmException;

/**
 * @author shahriar
 * @since 3/4/18
 */
@Controller
@RequestMapping(value = "/public/Signup")
public class SignupController {

    private UserDao userDao;

    @Autowired
    public SignupController(UserDao userDao) {
        this.userDao = userDao;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getSignUpForm(@ModelAttribute("user") User user) {

        return "signup";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String signUpUser(@ModelAttribute("user") @Valid User user, Errors errors, HttpServletRequest request) {

        if (errors.hasErrors()) {
            return "signup";
        }

        HttpSession session = request.getSession();

        try {
            user.setPassword(Util.getHashedPassword(user.getPassword()));
            user.setConfirmPassword(user.getPassword());

            userDao.addUser(user);

            session.setAttribute(Util.USER, userDao.getByUsername(user.getUsername()));

            return "redirect:/private/ViewAllMeal";
        } catch (NoSuchAlgorithmException e) {
            return "signup";
        }
    }
}
