package net.therap.mealmanagement.validator;

/**
 * @author shahriar
 * @since 3/4/18
 */

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;

public class FieldMatchValidator implements ConstraintValidator<FieldMatch, Object> {

    private String firstFieldName;
    private String secondFieldName;
    private String message;

    @Override
    public void initialize(final FieldMatch constraintAnnotation) {
        firstFieldName = constraintAnnotation.first();
        secondFieldName = constraintAnnotation.second();
        message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {

        final Object firstField = getValue(value, firstFieldName);
        final Object secondField = getValue(value, secondFieldName);

        boolean fieldsMatch;

        fieldsMatch = firstField != null && firstField.equals(secondField);

        if (!fieldsMatch) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(message)
                    .addPropertyNode(secondFieldName).addConstraintViolation();
            return false;
        }
        return true;
    }

    private Object getValue(Object object, String fieldName) {

        Field[] fields = object.getClass().getDeclaredFields();

        for (Field field : fields) {

            field.setAccessible(true);
            if (field.getName().equals(fieldName)) {
                try {
                    return field.get(object);
                } catch (IllegalAccessException e) {
                    return null;
                }
            }
        }
        return null;
    }
}