package net.therap.mealmanagement.validator;

/**
 * @author shahriar
 * @since 3/4/18
 */

import net.therap.mealmanagement.domain.Slot;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SlotMatchValidator implements ConstraintValidator<SlotMatch, Object> {

    private String message;

    @Override
    public void initialize(final SlotMatch constraintAnnotation) {
        message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {

        boolean slotMatch = value != null && contains(value.toString());

        if (!slotMatch) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(message)
                    .addConstraintViolation();
            return false;
        }
        return true;
    }

    private boolean contains(String value) {

        for (Slot slot : Slot.values()) {
            if (slot.name().equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }
}