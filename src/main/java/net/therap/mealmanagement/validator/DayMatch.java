package net.therap.mealmanagement.validator;

import net.therap.mealmanagement.util.Util;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author shahriar
 * @since 3/4/18
 */
@Target({TYPE, ANNOTATION_TYPE, FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = DayMatchValidator.class)
public @interface DayMatch {

    String message() default Util.VALID_ERROR_DAY;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
