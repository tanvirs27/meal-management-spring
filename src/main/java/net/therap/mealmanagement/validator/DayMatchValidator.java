package net.therap.mealmanagement.validator;

/**
 * @author shahriar
 * @since 3/4/18
 */

import net.therap.mealmanagement.domain.Day;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DayMatchValidator implements ConstraintValidator<DayMatch, Object> {

    private String message;

    @Override
    public void initialize(final DayMatch constraintAnnotation) {
        message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {

        boolean dayMatch = value != null && contains(value.toString());

        if (!dayMatch) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(message)
                    .addConstraintViolation();
            return false;
        }
        return true;
    }

    private boolean contains(String value) {

        for (Day day : Day.getConcernedDays()) {
            if (day.name().equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }
}