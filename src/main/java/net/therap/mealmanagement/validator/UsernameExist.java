package net.therap.mealmanagement.validator;

import net.therap.mealmanagement.util.Util;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author shahriar
 * @since 3/4/18
 */
@Target({TYPE, FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = UsernameExistValidator.class)
public @interface UsernameExist {

    String message() default Util.VALID_ERROR_USERNAME_EXIST;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
