package net.therap.mealmanagement.validator;

/**
 * @author shahriar
 * @since 3/4/18
 */

import net.therap.mealmanagement.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UsernameExistValidator implements ConstraintValidator<UsernameExist, Object> {

    @Autowired
    private UserDao userDao;

    private String message;

    @Override
    public void initialize(final UsernameExist constraintAnnotation) {
        message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {

        boolean isUsernameExist = value != null && exists(value.toString());

        if (isUsernameExist) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(message)
                    .addConstraintViolation();
            return false;
        }
        return true;
    }

    private boolean exists(String value) {

        return userDao.isExist(value);
    }
}