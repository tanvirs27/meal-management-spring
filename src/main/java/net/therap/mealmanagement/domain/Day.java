package net.therap.mealmanagement.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar
 * @since 2/11/18
 */
public enum Day {
    FRIDAY, SATURDAY, SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY;

    public static List<Day> getConcernedDays() {

        List<Day> days = new ArrayList<>();

        for (Day day : Day.values()) {

            if (day == Day.FRIDAY || day == Day.SATURDAY) {
                continue;
            }
            days.add(day);
        }
        return days;
    }
}
