package net.therap.mealmanagement.domain;

import net.therap.mealmanagement.util.Util;
import net.therap.mealmanagement.validator.FieldMatch;
import net.therap.mealmanagement.validator.UsernameExist;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar
 * @since 2/15/18
 */
@Entity
@Table(name = "user")
@FieldMatch(first = "password", second = "confirmPassword", message = Util.VALID_ERROR_PASSWORD)
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "full_name")
    @NotEmpty
    private String fullName;

    @NotEmpty
    @Size(min = 5, message = Util.VALID_ERROR_USERNAME)
    @UsernameExist
    private String username;

    @NotEmpty
    private String password;

    @Transient
    @NotEmpty
    private String confirmPassword;

    @NotEmpty
    private String designation;

    @Email
    @NotEmpty
    private String email;

    @OneToMany(mappedBy = "reviewer", cascade = CascadeType.ALL)
    private List<Review> allReviews;

    public User() {
        allReviews = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Review> getAllReviews() {
        return allReviews;
    }

    public void setAllReviews(List<Review> allReviews) {
        this.allReviews = allReviews;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
