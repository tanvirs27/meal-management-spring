package net.therap.mealmanagement.dao;

import net.therap.mealmanagement.domain.Item;
import net.therap.mealmanagement.domain.User;
import net.therap.mealmanagement.util.QueryParameter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar
 * @since 2/10/18
 */
@Component
public class ItemDao extends GenericDao<Item> {

    public ItemDao() {
        super(Item.class);
    }

    public Item getItem(String name, User user) {

        List<QueryParameter> parameters = new ArrayList<>();
        parameters.add(new QueryParameter("name", name));

        Item item = getSingleResultByProperty(parameters);

        if (item == null) {
            item = new Item();
            item.setName(name);
            item.setAddedBy(user);
        }

        return item;
    }

    public void updateItem(int id, String itemName) {

        Item item = getById(id);

        if (item != null) {
            item.setName(itemName);

            update(item);
        }
    }
}
