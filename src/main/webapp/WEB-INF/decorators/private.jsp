<%--
  User: shahriar
  Date: 3/8/18
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dec" uri="http://www.opensymphony.com/sitemesh/decorator" %>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><dec:title default="Meal Planner"/></title>

    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">

    <link href="<c:url value="/resources/css/font-awesome.min.css"/>" rel="stylesheet">

    <link href="<c:url value="/resources/css/main.css"/>" rel="stylesheet">

    <dec:head/>

</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="<c:url value="/"/>" class="site_title">Therap</a>
                </div>

                <div class="clearfix"></div>

                <br/>

                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            <li><a href="<c:url value="/private/ViewAllMeal"/>">View Meals</a></li>
                            <li><a href="<c:url value="/private/AddItemForm"/>">Add item To Meal</a></li>
                            <li><a href="<c:url value="/private/ShowAllReview"/>">View Reviews</a></li>
                            <li><a href="<c:url value="/private/AddReviewForm"/>">Give Review</a></li>
                        </ul>
                    </div>
                    <div class="menu_section">
                        <h3>Account</h3>
                        <ul class="nav side-menu">
                            <li><a href="<c:url value="/public/Logout"/>"> Logout </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a class="navbar-brand" href="<c:url value="/"/>">Meal Planner</a></li>
                    </ul>
                </nav>
            </div>
        </div>

        <dec:body/>

    </div>
</div>

<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>

</body>

</html>
