<%--
  User: shahriar
  Date: 3/8/18
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dec" uri="http://www.opensymphony.com/sitemesh/decorator" %>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><dec:title default="Meal Planner"/></title>

    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">

    <link href="<c:url value="/resources/css/font-awesome.min.css"/>" rel="stylesheet">

    <link href="<c:url value="/resources/css/main.css"/>" rel="stylesheet">

    <dec:head/>

</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav navbar-nav navbar-right">

                        <form method="get" action="<c:url value="/public/Login"/>">
                            <select name="locale">
                                <option value="en">English</option>
                                <option value="bn">Bangla</option>
                            </select>
                            <button type="submit">Change Language</button>
                        </form>

                    </div>
                </nav>
            </div>
        </div>

        <dec:body/>

    </div>
</div>

<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>

</body>

</html>
