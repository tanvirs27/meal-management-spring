<%--
  User: shahriar
  Date: 2/22/18
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:url var="updateItemUrl" value="/private/UpdateItem"/>

<html>

<head>
    <title>Update Item</title>
</head>

<body>

<div class="right_col" role="main">
    <div>
        <div class="page-title">
            <div class="title_left">
                <h3>Update an Item</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                    <div class="x_content">

                        <form:form commandName="item" action="${updateItemUrl}" method="post"
                                   cssClass="form-horizontal form-label-left">

                            <div class="clearfix"></div>

                            <form:errors path="*"/>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Id</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <form:input path="id" cssClass="form-control col-md-7 col-xs-12"
                                                readonly="true" value="${id}"/>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Added By</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <form:input path="addedBy.username"
                                                cssClass="form-control col-md-7 col-xs-12"
                                                readonly="true" value="${addedBy.username}"/>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <form:input path="name" cssClass="form-control col-md-7 col-xs-12"
                                                value="${name}"/>
                                    <form:errors path="name" cssStyle="color: red"/>
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <form:button type="submit" class="btn btn-success">Update</form:button>
                                </div>
                            </div>

                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
