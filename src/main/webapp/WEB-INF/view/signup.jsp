<%--
  User: shahriar
  Date: 2/22/18
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>

<head>
    <title>Signup</title>
</head>

<body>

<div class="right_col" role="main">
    <div>
        <div class="page-title">
            <div class="title_left">
                <h3>Sign up to Meal Planner</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                    <div class="x_content">

                        <form:form cssClass="form-horizontal form-label-left"
                                   method="post" commandName="user">

                            <div class="clearfix"></div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Full Name</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <form:input path="fullName" cssClass="form-control col-md-7 col-xs-12"
                                                placeholder="Enter your full name"/>
                                    <form:errors path="fullName" cssStyle="color: red"/>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Designation</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <form:input path="designation" cssClass="form-control col-md-7 col-xs-12"
                                                placeholder="Enter your designation"/>
                                    <form:errors path="designation" cssStyle="color: red"/>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <form:input type="email" path="email"
                                                cssClass="form-control col-md-7 col-xs-12"
                                                placeholder="Enter your designation"/>
                                    <form:errors path="email" cssStyle="color: red"/>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Username</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <form:input path="username" cssClass="form-control col-md-7 col-xs-12"
                                                placeholder="Enter your username"/>
                                    <form:errors path="username" cssStyle="color: red"/>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <form:password path="password" cssClass="form-control col-md-7 col-xs-12"
                                                   placeholder="Enter your password"/>
                                    <form:errors path="password" cssStyle="color: red"/>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                    Confirm Password</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <form:password path="confirmPassword"
                                                   cssClass="form-control col-md-7 col-xs-12"
                                                   placeholder="Re-type your password"/>
                                    <form:errors path="confirmPassword" cssStyle="color: red"/>
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Signup</button>
                                </div>
                            </div>

                        </form:form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
