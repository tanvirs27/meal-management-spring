<%--
  User: shahriar
  Date: 2/22/18
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <title>Login</title>
</head>

<body>

<div class="right_col" role="main">
    <div>
        <div class="page-title">
            <div class="title_left">
                <h3><spring:message code="label.header"/></h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                    <div class="x_content">

                        <form:form commandName="user" method="post" cssClass="form-horizontal form-label-left">

                            <div class="clearfix"></div>

                            <c:if test="${not empty error}">
                                <div class="form-group" style="color: red">
                                    <h4>${error}</h4>
                                </div>
                            </c:if>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                    <spring:message code="label.username"/></label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <form:input path="username" cssClass="form-control col-md-7 col-xs-12"
                                                placeholder="Enter your username"/>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                    <spring:message code="label.password"/></label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <form:password path="password"
                                                   class="form-control col-md-7 col-xs-12"
                                                   placeholder="Enter your password"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">
                                        <spring:message code="label.login"/></button>
                                </div>
                            </div>

                        </form:form>
                        <h4>Don't have an account? Sign up <a href="<c:url value="/public/Signup"/>">here</a>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
