<%--
  User: shahriar
  Date: 2/22/18
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:url var="showReviewByMealUrl" value="/private/ShowReviewByMeal"/>
<spring:url var="showReviewByUserUrl" value="/private/ShowReviewByUser"/>

<html>

<head>
    <title>Reviews</title>
</head>

<body>

<div class="right_col" role="main">
    <div>
        <div class="page-title">
            <div class="title_left">
                <h3>Reviews</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 pull-right">
                    <a href="<c:url value="/private/ShowAllReview"/>" class="btn btn-block btn-primary">
                        View All</a>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                    <div class="x_content">

                        <form:form commandName="review" action="${showReviewByUserUrl}" method="post"
                                   cssClass="form-inline form-label-left">

                            <div class="clearfix"></div>

                            <div class="form-group">
                                <label class="control-label col-md-6 col-sm-6 col-xs-12">Reviewer Name</label>

                                <div class="col-md-6 col-sm-6 col-xs-12 ">
                                    <form:input path="reviewer.username"
                                                cssClass="form-control col-md-7 col-xs-12"
                                                placeholder="Type a reviewer name"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-2 col-sm-2 col-xs-2 col-md-offset-2">
                                    <form:button type="submit" class="btn btn-success">
                                        Filter By Reviewer</form:button>
                                </div>
                            </div>

                        </form:form>

                        <form:form commandName="review" action="${showReviewByMealUrl}" method="post"
                                   cssClass="form-inline form-label-left">

                            <div class="clearfix"></div>

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Day</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <form:select path="meal.day" cssClass="form-control col-md-7 col-xs-12">

                                        <form:option value="">Choose a day</form:option>
                                        <form:options items="${dayList}"/>

                                    </form:select>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Slot</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <form:select path="meal.slot" cssClass="form-control col-md-7 col-xs-12">

                                        <form:option value="">Choose a slot</form:option>
                                        <form:options items="${slotList}"/>

                                    </form:select>

                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-md-2 col-sm-2 col-xs-2 col-md-offset-2">
                                    <form:button type="submit" class="btn btn-success">
                                        Filter By Meals</form:button>
                                </div>
                            </div>

                        </form:form>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">

                    <div class="clearfix"></div>

                    <div class="x_content">

                        <table class="table table-striped projects">
                            <thead>
                            <tr>
                                <th style="width: 20%">Meal</th>
                                <th style="width: 20%">Reviewer</th>
                                <th style="width: 20%">Time</th>
                                <th style="width: 20%">Rating</th>
                                <th style="width: 20%">Comment</th>
                            </tr>
                            </thead>
                            <tbody id="table_body">

                            <c:forEach var="review" items="${requestScope.allReviews}">
                                <tr>

                                    <td><c:out value="${review.meal.day} ${review.meal.slot}"/>
                                    </td>
                                    <td><c:out value="${review.reviewer.username}"/>
                                    </td>
                                    <td><c:out value="${review.time}"/>
                                    </td>
                                    <td><c:out value="${review.rating}"/>
                                    </td>
                                    <td><c:out value="${review.comment}"/>
                                    </td>

                                </tr>
                            </c:forEach>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
