<%--
  User: shahriar
  Date: 2/22/18
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:url var="addItemUrl" value="/private/AddItem"/>

<html>

<head>
    <title>Add Item</title>
</head>

<body>

<div class="right_col" role="main">
    <div>
        <div class="page-title">
            <div class="title_left">
                <h3>Add an Item to a Meal</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                    <div class="x_content">

                        <form:form commandName="meal" action="${addItemUrl}" method="post"
                                   cssClass="form-horizontal form-label-left">

                            <div class="clearfix"></div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Day</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <c:choose>

                                        <c:when test="${not empty meal.day}">
                                            <form:input path="day" readonly="true"
                                                        cssClass="form-control col-md-7 col-xs-12"
                                                        value="${meal.day}"/>
                                        </c:when>

                                        <c:otherwise>

                                            <form:select path="day" cssClass="form-control col-md-7 col-xs-12">

                                                <form:option value="">Choose a day</form:option>
                                                <form:options items="${dayList}"/>

                                            </form:select>
                                        </c:otherwise>

                                    </c:choose>

                                    <form:errors path="day" cssStyle="color: red"/>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Slot</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <c:choose>

                                        <c:when test="${not empty meal.slot}">

                                            <form:input path="slot" readonly="true"
                                                        cssClass="form-control col-md-7 col-xs-12"
                                                        value="${meal.slot}"/>
                                        </c:when>

                                        <c:otherwise>
                                            <form:select path="slot" cssClass="form-control col-md-7 col-xs-12">

                                                <form:option value="">Choose a slot</form:option>
                                                <form:options items="${slotList}"/>

                                            </form:select>
                                        </c:otherwise>

                                    </c:choose>

                                    <form:errors path="slot" cssStyle="color: red"/>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Item Name</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <form:input path="item.name" cssClass="form-control col-md-7 col-xs-12"
                                                placeholder="Name of an item"/>
                                    <form:errors path="item.name" cssStyle="color: red"/>
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <form:button type="submit" class="btn btn-success">Add</form:button>
                                </div>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
