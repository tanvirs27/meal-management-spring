<%--
  User: shahriar
  Date: 2/22/18
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:url var="viewMealBySlotUrl" value="/private/ViewMealBySlot"/>
<spring:url var="addItemUrl" value="/private/AddItemForm"/>
<spring:url var="removeItemUrl" value="/private/RemoveItem"/>
<spring:url var="updateItemUrl" value="/private/UpdateItemForm"/>

<html>
<head>
    <title>View Meals</title>
</head>

<body>

<div class="right_col" role="main">
    <div>
        <div class="page-title">
            <div class="title_left">
                <h3>View Meals</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 pull-right">
                    <a href="<c:url value="/private/ViewAllMeal"/>"
                       class="btn btn-block btn-primary">View All</a>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                    <div class="x_content">


                        <form:form action="${viewMealBySlotUrl}" commandName="meal"
                                   method="post" cssClass="form-inline form-label-left">

                            <div class="clearfix"></div>

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Day</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <form:select path="day" cssClass="form-control col-md-7 col-xs-12">

                                        <form:option value="">Choose a day</form:option>
                                        <form:options items="${dayList}"/>

                                    </form:select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Slot</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <form:select path="slot" cssClass="form-control col-md-7 col-xs-12">

                                        <form:option value="">Choose a slot</form:option>
                                        <form:options items="${slotList}"/>

                                    </form:select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-2 col-sm-2 col-xs-2 col-md-offset-2">
                                    <form:button type="submit" class="btn btn-success">Search</form:button>
                                </div>
                            </div>

                        </form:form>
                    </div>
                </div>
            </div>
        </div>

        <c:forEach var="meal" items="${requestScope.allMeals}">
            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">

                        <div class="title_right">
                            <div class="col-md-2 col-sm-2 col-xs-2 pull-right">
                                <form:form commandName="meal" method="post" action="${addItemUrl}">
                                    <form:hidden path="day" value="${meal.day}"/>
                                    <form:hidden path="slot" value="${meal.slot}"/>

                                    <form:button type="submit" class="btn btn-block btn-primary">
                                        Add Item</form:button>
                                </form:form>
                            </div>
                        </div>

                        <div class="x_title list-inline">
                            <h4><c:out value="${meal.day} ${meal.slot}"/>
                            </h4>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <fmt:formatDate var="modificationTime"
                                            value="${meal.lastModificationTime}" type="both"
                                            pattern="dd-MM-yyyy hh:mm"/>

                            <p><c:out value="Last Modified By
                                        ${meal.modifiedBy.username} at ${modificationTime}"/>
                            </p>

                            <table class="table table-striped projects">
                                <thead>
                                <tr>
                                    <th style="width: 20%">Item Name</th>
                                    <th style="width: 20%">Added By</th>
                                    <th style="width: 20%">Addition Time</th>
                                    <th style="width: 20%">Remove</th>
                                    <th style="width: 20%">Update</th>
                                </tr>
                                </thead>
                                <tbody id="table_body">

                                <c:forEach var="item" items="${meal.items}">

                                    <fmt:formatDate var="additionTime"
                                                    value="${item.additionTime}" type="both"
                                                    pattern="dd-MM-yyyy hh:mm"/>

                                    <tr>
                                        <td><c:out value="${item.name}"/>
                                        </td>
                                        <td><c:out value="${item.addedBy.username}"/>
                                        </td>
                                        <td><c:out value="${additionTime}"/>
                                        </td>
                                        <td>
                                            <form:form commandName="meal" method="post"
                                                       action="${removeItemUrl}">

                                                <form:hidden path="id" value="${meal.id}"/>
                                                <form:hidden path="item" value="${item.id}"/>
                                                <form:button type="submit" class="btn btn-danger btn-xs">
                                                    Remove</form:button>

                                            </form:form>
                                        </td>

                                        <td>
                                            <form:form commandName="meal" method="post"
                                                       action="${updateItemUrl}">

                                                <form:hidden path="item" value="${item.id}"/>

                                                <form:button type="submit" class="btn btn-info btn-xs">
                                                    Update</form:button>

                                            </form:form>
                                        </td>
                                    </tr>
                                </c:forEach>

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>

</body>
</html>
