<%--
  User: shahriar
  Date: 2/22/18
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:url var="addReviewUrl" value="/private/AddReview"/>

<html>

<head>
    <title>Add Review</title>
</head>

<body>

<div class="right_col" role="main">
    <div>
        <div class="page-title">
            <div class="title_left">
                <h3>Give a review to a Meal</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                    <div class="x_content">

                        <form:form commandName="review" action="${addReviewUrl}" method="post"
                                   cssClass="form-horizontal form-label-left">

                            <div class="clearfix"></div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Day</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <form:select path="meal.day" cssClass="form-control col-md-7 col-xs-12">

                                        <form:option value="">Choose a day</form:option>
                                        <form:options items="${dayList}"/>

                                    </form:select>
                                    <form:errors path="meal.day" cssStyle="color: #ff0000"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Slot</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <form:select path="meal.slot" cssClass="form-control col-md-7 col-xs-12">

                                        <form:option value="">Choose a slot</form:option>
                                        <form:options items="${slotList}"/>

                                    </form:select>
                                    <form:errors path="meal.slot" cssStyle="color: #ff0000"/>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Rating</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <form:input type="number" path="rating"
                                                cssClass="form-control col-md-7 col-xs-12"
                                                placeholder="Give a rating"/>
                                    <form:errors path="rating" cssStyle="color: #ff0000"/>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Comment</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <form:input path="comment" cssClass="form-control col-md-7 col-xs-12"
                                                placeholder="Give a comment"/>
                                    <form:errors path="comment" cssStyle="color: #ff0000"/>
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <form:button type="submit" class="btn btn-success">Add Review</form:button>
                                </div>
                            </div>

                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
